package ru.t1.kruglikov.tm;

import ru.t1.kruglikov.tm.constant.ArgumentConst;
import ru.t1.kruglikov.tm.constant.CommandConst;
import static ru.t1.kruglikov.tm.constant.CommandConst.*;
import java.util.Scanner;
import ru.t1.kruglikov.tm.utils.NumberUtil;

public class Application {
    private static void displayHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s, %s : Show about program. \n", CommandConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s, %s : Show program version. \n", CommandConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s : Show list arguments. \n", CommandConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s, %s : Show system info. \n", CommandConst.INFO, ArgumentConst.INFO);
        System.out.printf("%s : Close application. \n", EXIT);
    }

    private static void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    private static void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Kruglikov Andrey");
        System.out.println("akruglikov@t1-consulting.ru");
    }

    private static void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
        System.exit(0);
    }

    private static void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.ABOUT:
                displayAbout();
                break;
            case ArgumentConst.VERSION:
                displayVersion();
                break;
            case ArgumentConst.HELP:
                displayHelp();
                break;
            case ArgumentConst.INFO:
                displayInfo();
                break;
            default:
                displayArgumentError();
                break;
        }
    }

    private static void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.ABOUT:
                displayAbout();
                break;
            case CommandConst.VERSION:
                displayVersion();
                break;
            case CommandConst.HELP:
                displayHelp();
                break;
            case CommandConst.INFO:
                displayInfo();
                break;
            case CommandConst.EXIT:
                exit();
                break;
            default:
                displayCommandError();
                break;
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void displayArgumentError() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    private static void displayCommandError() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
        System.exit(1);
    }

    private static void displayInfo() {
        System.out.println("[INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void main(final String[] args) {
        processArguments(args);
        System.out.println("** WELCOME TASK MANAGER **");
        final Scanner scanner = new Scanner(System.in);
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }
}
